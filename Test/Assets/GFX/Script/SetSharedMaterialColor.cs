﻿using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

    [ActionCategory(ActionCategory.Material)]
    public class SetSharedMaterialColor : FsmStateAction
    {
        public Material sharedMaterial = null;
        public string materialProperty = "_Color";
        public FsmColor color = default;
        public bool everyFrame = false; 

        public override void OnEnter()
        {
            sharedMaterial.SetColor(materialProperty, color.Value);
            if(!everyFrame)
            {
                Finish();
            }
        }

        public override void OnUpdate()
        {
            if(everyFrame)
            {
            sharedMaterial.SetColor(materialProperty, color.Value);
            }
        }
    }

}
